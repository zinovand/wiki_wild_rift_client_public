package tjv.semestral_work.wiki_wild_rift_client.ui;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/index")
public class IndexWebController {
    @GetMapping
    public String getIndex (Model model){
        return "index";
    }
}
