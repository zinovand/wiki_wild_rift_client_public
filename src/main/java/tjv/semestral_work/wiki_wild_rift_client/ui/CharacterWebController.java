package tjv.semestral_work.wiki_wild_rift_client.ui;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tjv.semestral_work.wiki_wild_rift_client.data.CharacterClient;
import tjv.semestral_work.wiki_wild_rift_client.model.CharacterDTO;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller
@RequestMapping("/character")
public class CharacterWebController {

    private final CharacterClient characterClient;

    String message = "";
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public CharacterWebController(CharacterClient characterClient) {
        this.characterClient = characterClient;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    //GET ALL
    @GetMapping
    public String getAll (Model model){
        model.addAttribute("characterList", characterClient.getAll());
        model.addAttribute("statusCharacter", message);
        return "characterAll";
    }
    ///

    //ADD NEW
    @GetMapping("/add")
    public String add(Model model){
        model.addAttribute("new_character", new CharacterDTO());
        model.addAttribute("statusClass", message);
        return "characterAdd";
    }

    @PostMapping("/add")
    public String addSend(@ModelAttribute CharacterDTO new_character, Model model){
        characterClient.create(new_character)
                .subscribe(
                        x ->  setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Add Character\"" + " Status: SUCCESS"),
                        e -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Add Character\"" + " Status: ERROR")
                );
        return "redirect:/character";

    }
    ///

    //UPDATE
    @GetMapping("/edit")
    public String edit(@RequestParam Integer id, Model model){
        characterClient.setCurrentID(id);
        model.addAttribute("update_character", characterClient.getOne());
        return "characterEdit";
    }

    @PostMapping("/edit")
    public String editSend(@ModelAttribute CharacterDTO update_character, Model model){
        characterClient.update(update_character)
                .subscribe(
                        x -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Edit Character\"" + " Status: SUCCESS"),
                        e -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Edit Character\"" + " Status: ERROR")
                );
        characterClient.setCurrentID(null);
        return "redirect:/character";
    }
    ///

    //DELETE
    @GetMapping("/delete")
    public String delete(@RequestParam Integer id, Model model) {
        characterClient.setCurrentID(id);
        characterClient.delete()
                .subscribe(
                        x -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Delete Character\"" + " Status: SUCCESS"),
                        e -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Delete Character\"" + " Status: ERROR")
                );
        characterClient.setCurrentID(null);
        return "redirect:/character";
    }
    ///

}
