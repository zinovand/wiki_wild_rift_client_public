package tjv.semestral_work.wiki_wild_rift_client.ui;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tjv.semestral_work.wiki_wild_rift_client.data.ClassClient;
import tjv.semestral_work.wiki_wild_rift_client.model.ClassDTO;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller
@RequestMapping("/class")
public class ClassWebController {

    private final ClassClient classClient;

    String message = "";

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public ClassWebController(ClassClient classClient) {
        this.classClient = classClient;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    //GET ALL
    @GetMapping
    public String getAll (Model model){
        model.addAttribute("classList", classClient.getAll());
        model.addAttribute("statusClass", message);

        return "classAll";
    }
    ///

    //ADD NEW
    @GetMapping("/add")
    public String add(Model model){
        model.addAttribute("new_class", new ClassDTO());
        model.addAttribute("statusClass", message);
        return "classAdd";
    }

    @PostMapping("/add")
    public String addSend(@ModelAttribute ClassDTO new_class, Model model){
        classClient.create(new_class)
                .subscribe(
                        x ->  setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Add Class\"" + " Status: SUCCESS"),
                        e -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Add Class\"" + " Status: ERROR")
                );
        return "redirect:/class";

    }
    ///

    //UPDATE
    @GetMapping("/edit")
    public String edit(@RequestParam Integer id, Model model){
        classClient.setCurrentID(id);
        model.addAttribute("update_class", classClient.getOne());
        return "classEdit";
    }

    @PostMapping("/edit")
    public String editSend(@ModelAttribute ClassDTO update_class, Model model){
        classClient.update(update_class)
                .subscribe(
                        x -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Edit Class\"" + " Status: SUCCESS"),
                        e -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Edit Class\"" + " Status: ERROR")
                );
        classClient.setCurrentID(null);
        return "redirect:/class";
    }
    ///

    //DELETE
    @GetMapping("/delete")
    public String delete(@RequestParam Integer id, Model model) {
        classClient.setCurrentID(id);
        classClient.delete()
                .subscribe(
                        x -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Delete Class\"" + " Status: SUCCESS"),
                        e -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Delete Class\"" + " Status: ERROR")
                );
        classClient.setCurrentID(null);
        return "redirect:/class";
    }
    ///

}
