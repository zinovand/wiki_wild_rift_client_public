package tjv.semestral_work.wiki_wild_rift_client.ui;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tjv.semestral_work.wiki_wild_rift_client.data.SkillClient;
import tjv.semestral_work.wiki_wild_rift_client.model.SkillDTO;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller
@RequestMapping("/skill")
public class SkillWebController {

    private final SkillClient skillClient;

    String message = "";
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public SkillWebController(SkillClient skillClient) {
        this.skillClient = skillClient;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    //GET ALL
    @GetMapping
    public String getAll (Model model){
        model.addAttribute("skillList", skillClient.getAll());
        model.addAttribute("statusSkill", message);
        return "skillAll";
    }
    ///

    //ADD NEW
    @GetMapping("/add")
    public String add(Model model){
        model.addAttribute("new_skill", new SkillDTO());
        model.addAttribute("statusClass", message);
        return "skillAdd";
    }

    @PostMapping("/add")
    public String addSend(@ModelAttribute SkillDTO new_skill, Model model){
        if (new_skill.getIs_passive() == null){
            new_skill.setIs_passive(false);
        }
        skillClient.create(new_skill)
                .subscribe(
                        x ->  setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Add Skill\"" + " Status: SUCCESS"),
                        e -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Add Skill\"" + " Status: ERROR")
                );
        return "redirect:/skill";

    }
    ///

    //UPDATE
    @GetMapping("/edit")
    public String edit(@RequestParam Integer id, Model model){
        skillClient.setCurrentID(id);
        model.addAttribute("update_skill", skillClient.getOne());
        return "skillEdit";
    }

    @PostMapping("/edit")
    public String editSend(@ModelAttribute SkillDTO update_skill, Model model){
        skillClient.update(update_skill)
                .subscribe(
                        x -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Edit Skill\"" + " Status: SUCCESS"),
                        e -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Edit Skill\"" + " Status: ERROR")
                );
        skillClient.setCurrentID(null);
        return "redirect:/skill";
    }
    ///

    //DELETE
    @GetMapping("/delete")
    public String delete(@RequestParam Integer id, Model model) {
        skillClient.setCurrentID(id);
        skillClient.delete()
                .subscribe(
                        x -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Delete Skill\"" + " Status: SUCCESS"),
                        e -> setMessage("Time: " + LocalDateTime.now().format(formatter) + " Operation: \"Delete Skill\"" + " Status: ERROR")
                );
        skillClient.setCurrentID(null);
        return "redirect:/skill";
    }
    ///

}
