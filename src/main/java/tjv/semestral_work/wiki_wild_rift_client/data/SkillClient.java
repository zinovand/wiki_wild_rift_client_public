package tjv.semestral_work.wiki_wild_rift_client.data;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tjv.semestral_work.wiki_wild_rift_client.model.SkillDTO;

@Component
public class SkillClient {
    private final WebClient skillWebClient;
    private Integer currentID;

    public Integer getCurrentID(){
        return currentID;
    }

    public void setCurrentID(Integer ID){
        this.currentID = ID;
        if(ID != null)
            try {
                getOne();
            } catch (WebClientException e) {
                this.currentID = null;
                throw e;
            }
    }

    public SkillClient(@Value("${backend_url}") String backendUrl) {
        skillWebClient = WebClient.create(backendUrl + "/skill");
    }

    public Flux<SkillDTO> getAll() {
        return skillWebClient.get()
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(SkillDTO.class);
    }

        public Mono<SkillDTO> getOne() {
        if(currentID == null) {throw new IllegalStateException("current id not set");}
        return skillWebClient.get()
                .uri("/{id}", currentID)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(SkillDTO.class);
    }

    public Mono<SkillDTO> create(SkillDTO new_skill) {
             return skillWebClient.post()
                     .contentType(MediaType.APPLICATION_JSON)
                     .accept(MediaType.APPLICATION_JSON)
                     .bodyValue(new_skill)
                     .retrieve()
                     .bodyToMono(SkillDTO.class);
    }


    public Mono<ResponseEntity<Void>> update(SkillDTO update_skill) {
        if(currentID == null){throw new IllegalStateException("current id not set");}

            return skillWebClient.put()
                .uri("/{id}", currentID)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(update_skill)
                .retrieve()
                .toBodilessEntity();
    }

    public Mono<ResponseEntity<Void>> delete() {
        if(currentID == null) {throw new IllegalStateException("current id not set");}

        return skillWebClient.delete()
                .uri("/{id}", currentID)
                .retrieve()
                .toBodilessEntity();
    }

}
