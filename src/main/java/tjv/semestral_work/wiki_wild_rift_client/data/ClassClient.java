package tjv.semestral_work.wiki_wild_rift_client.data;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tjv.semestral_work.wiki_wild_rift_client.model.ClassDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;

@Component
public class ClassClient {
    private final WebClient classWebClient;
    private Integer currentID;

    public Integer getCurrentID(){
        return currentID;
    }

    public void setCurrentID(Integer ID){
        this.currentID = ID;
        if(ID != null)
            try {
                getOne();
            } catch (WebClientException e) {
                this.currentID = null;
                throw e;
            }
    }

    public ClassClient(@Value("${backend_url}") String backendUrl) {
        classWebClient = WebClient.create(backendUrl + "/class");
    }

    public Flux<ClassDTO> getAll() {
        return classWebClient.get()
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(ClassDTO.class);
    }

    public Mono<ClassDTO> create(ClassDTO new_class) {
             return classWebClient.post()
                     .contentType(MediaType.APPLICATION_JSON)
                     .accept(MediaType.APPLICATION_JSON)
                     .bodyValue(new_class)
                     .retrieve()
                     .bodyToMono(ClassDTO.class);
    }

    public Mono<ClassDTO> getOne() {
        if(currentID == null)
            throw new IllegalStateException("current id not set");
        return classWebClient.get()
                .uri("/{id}", currentID)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(ClassDTO.class);
    }

    public Mono<ResponseEntity<Void>> update(ClassDTO update_class) {
        if(currentID == null){throw new IllegalStateException("current id not set");}

            return classWebClient.put()
                .uri("/{id}", currentID)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(update_class)
                .retrieve()
                .toBodilessEntity();
    }

    public Mono<ResponseEntity<Void>> delete() {
        if(currentID == null) {throw new IllegalStateException("current id not set");}

        return classWebClient.delete()
                .uri("/{id}", currentID)
                .retrieve()
                .toBodilessEntity();
    }

}
