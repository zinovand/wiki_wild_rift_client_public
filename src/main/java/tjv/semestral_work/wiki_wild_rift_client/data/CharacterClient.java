package tjv.semestral_work.wiki_wild_rift_client.data;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tjv.semestral_work.wiki_wild_rift_client.model.CharacterDTO;

@Component
public class CharacterClient {
    private final WebClient characterWebClient;
    private Integer currentID;

    public Integer getCurrentID(){
        return currentID;
    }

    public void setCurrentID(Integer ID){
        this.currentID = ID;
        if(ID != null)
            try {
                getOne();
            } catch (WebClientException e) {
                this.currentID = null;
                throw e;
            }
    }

    public CharacterClient(@Value("${backend_url}") String backendUrl) {
        characterWebClient = WebClient.create(backendUrl + "/character");
    }

    public Flux<CharacterDTO> getAll() {
        return characterWebClient.get()
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(CharacterDTO.class);
    }

        public Mono<CharacterDTO> getOne() {
        if(currentID == null) {throw new IllegalStateException("current id not set");}
        return characterWebClient.get()
                .uri("/{id}", currentID)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(CharacterDTO.class);
    }

    public Mono<CharacterDTO> create(CharacterDTO new_character) {
             return characterWebClient.post()
                     .contentType(MediaType.APPLICATION_JSON)
                     .accept(MediaType.APPLICATION_JSON)
                     .bodyValue(new_character)
                     .retrieve()
                     .bodyToMono(CharacterDTO.class);
    }


    public Mono<ResponseEntity<Void>> update(CharacterDTO update_character) {
        if(currentID == null){throw new IllegalStateException("current id not set");}

            return characterWebClient.put()
                .uri("/{id}", currentID)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(update_character)
                .retrieve()
                .toBodilessEntity();
    }

    public Mono<ResponseEntity<Void>> delete() {
        if(currentID == null) {throw new IllegalStateException("current id not set");}

        return characterWebClient.delete()
                .uri("/{id}", currentID)
                .retrieve()
                .toBodilessEntity();
    }

}
