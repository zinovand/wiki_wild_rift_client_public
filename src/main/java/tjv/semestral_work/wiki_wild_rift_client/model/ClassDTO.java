package tjv.semestral_work.wiki_wild_rift_client.model;

import com.fasterxml.jackson.annotation.JsonView;

public class ClassDTO {
        @JsonView
        public Integer id;

        @JsonView
        public String name;

        @JsonView
        public String heroes;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getHeroes() {
            return heroes;
        }

        public void setHeroes(String heroes) {
            this.heroes = heroes;
        }

        public ClassDTO() {}
}
