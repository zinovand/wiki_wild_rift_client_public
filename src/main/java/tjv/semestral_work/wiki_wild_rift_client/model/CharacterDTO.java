package tjv.semestral_work.wiki_wild_rift_client.model;

import com.fasterxml.jackson.annotation.JsonView;

public class CharacterDTO {
    @JsonView
    public Integer id;

    @JsonView
    public String name;

    @JsonView
    public Integer health;

    @JsonView
    public Integer move_speed;

    @JsonView
    public Double attack_speed;

    @JsonView
    public Integer physical_damage;

    @JsonView
    public Integer magic_damage;

    @JsonView
    public Integer physical_armor;

    @JsonView
    public Integer magic_armor;

    @JsonView
    public String hero_class;

    @JsonView
    public String hero_skills;

    @JsonView
    public String weak_vs;

    @JsonView
    public String strong_vs;

    public CharacterDTO() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHealth() {
        return health;
    }

    public void setHealth(Integer health) {
        this.health = health;
    }

    public Integer getMove_speed() {
        return move_speed;
    }

    public void setMove_speed(Integer move_speed) {
        this.move_speed = move_speed;
    }

    public Double getAttack_speed() {
        return attack_speed;
    }

    public void setAttack_speed(Double attack_speed) {
        this.attack_speed = attack_speed;
    }

    public Integer getPhysical_damage() {
        return physical_damage;
    }

    public void setPhysical_damage(Integer physical_damage) {
        this.physical_damage = physical_damage;
    }

    public Integer getMagic_damage() {
        return magic_damage;
    }

    public void setMagic_damage(Integer magic_damage) {
        this.magic_damage = magic_damage;
    }

    public Integer getPhysical_armor() {
        return physical_armor;
    }

    public void setPhysical_armor(Integer physical_armor) {
        this.physical_armor = physical_armor;
    }

    public Integer getMagic_armor() {
        return magic_armor;
    }

    public void setMagic_armor(Integer magic_armor) {
        this.magic_armor = magic_armor;
    }

    public String getHero_class() {
        return hero_class;
    }

    public void setHero_class(String hero_class) {
        this.hero_class = hero_class;
    }

    public String getHero_skills() {
        return hero_skills;
    }

    public void setHero_skills(String hero_skills) {
        this.hero_skills = hero_skills;
    }

    public String getWeak_vs() {
        return weak_vs;
    }

    public void setWeak_vs(String weak_vs) {
        this.weak_vs = weak_vs;
    }

    public String getStrong_vs() {
        return strong_vs;
    }

    public void setStrong_vs(String strong_vs) {
        this.strong_vs = strong_vs;
    }
}
