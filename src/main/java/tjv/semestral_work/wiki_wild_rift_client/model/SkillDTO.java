package tjv.semestral_work.wiki_wild_rift_client.model;

import com.fasterxml.jackson.annotation.JsonView;

public class SkillDTO {
    @JsonView
    public Integer id;

    @JsonView
    public String name;

    @JsonView
    public String effect;

    @JsonView
    public Integer action_time;

    @JsonView
    public Integer cooldown;

    @JsonView
    public Integer cost;

    @JsonView
    public Boolean is_passive;

    @JsonView
    public String owner;

    public SkillDTO() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public Integer getAction_time() {
        return action_time;
    }

    public void setAction_time(Integer action_time) {
        this.action_time = action_time;
    }

    public Integer getCooldown() {
        return cooldown;
    }

    public void setCooldown(Integer cooldown) {
        this.cooldown = cooldown;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Boolean getIs_passive() {
        return is_passive;
    }

    public void setIs_passive(Boolean is_passive) {
        this.is_passive = is_passive;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
